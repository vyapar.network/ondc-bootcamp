// Copyright (c) 2022 Ashish Banerjee. All rights reserved.
// Use of this source code is governed by the Apache License, Version 2.0
// that can be found in the LICENSE file.

package main

import (
	"crypto/ed25519"
	cryptorand "crypto/rand"
	"encoding/base32"
	"encoding/base64"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"maze.io/x/crypto/x25519"
)

type OndcConfig struct {
	Country       string    `json:"country"`
	City          string    `json:"city"`
	Type          string    `json:"type"`
	SubscriberID  string    `json:"subscriber_id"`
	SubscriberURL string    `json:"subscriber_url"`
	Domain        string    `json:"domain"`
	SigningPubKey []byte    `json:"signing_public_key"` //crypto.PublicKey
	EncrPubKey    []byte    `json:"encr_public_key"`    //crypto.PublicKey
	Created       time.Time `json:"created"`
	ValidFrom     time.Time `json:"valid_from"`
	ValidUntil    time.Time `json:"valid_until"`
}

var country, city, typ, subsID, subsURL, domain, prefix string
var durationMon int

func init() {
	flag.StringVar(&country, "country", "IND", "3 letter country code, like IND")
	flag.StringVar(&city, "city", "*", "STD city code, example, std:0120")
	flag.StringVar(&typ, "type", "BPP", "application type BPP or BAP")
	flag.StringVar(&subsID, "subsid", "bpp.ondc.vyapar.network", "your subscriber id for ONDC registraction, should be your domain/subdomain")
	flag.StringVar(&subsURL, "subsurl", "https://bpp.ondc.vyapar.network/api", "subscriber's BPP/BAP OpenAPI end point")
	flag.StringVar(&domain, "domain", "nic2004:52110", "ONDC Network domain, example nic2004:52110")
	flag.StringVar(&prefix, "prefix", "", "prefix of the files to generate")

	flag.IntVar(&durationMon, "months", 12, "validity period of the public keys in months, default 12 ")

}

func main() {
	flag.Parse()
	cfg := fill()
	sigKey, encKey, err := genKeys(cfg, durationMon)
	if err != nil {
		log.Fatal(err)
	}
	err = saveFiles(prefix, cfg, sigKey, encKey)
	if err != nil {
		log.Fatal(err)
	}

}

const (
	defaPrefix    = "ondc-cfg-"
	postfixCfg    = "-cfg.json"
	postfixSigKey = "-secret-ed5119.key"
	postfixEncKey = "-secret-x5119.pem"
)

func saveFiles(prefix string, cfg *OndcConfig, sigKey, encKey []byte) error {
	if prefix == "" {
		pfix := fmt.Sprintf("%v", time.Now().Unix())
		pfix = strings.Trim(pfix, "0")
		prefix = defaPrefix + base32.StdEncoding.WithPadding(base32.NoPadding).EncodeToString([]byte(pfix))
	}
	jsonFl := prefix + postfixCfg
	sigKeyFl := prefix + postfixSigKey
	encKeyFl := prefix + postfixEncKey
	// check if files exist
	if _, err := os.Stat(jsonFl); err == nil || !errors.Is(err, os.ErrNotExist) {
		return fmt.Errorf("%v already exists", jsonFl)
	}
	if jm, err := json.Marshal(cfg); err != nil {
		return nil
	} else {
		if erx := write(jsonFl, string(jm)); erx != nil {
			return erx
		}
	}
	if _, err := os.Stat(sigKeyFl); err == nil || !errors.Is(err, os.ErrNotExist) {
		return fmt.Errorf("%v already exists", sigKeyFl)
	}
	//json.Marshal(sigKey)
	if erx := write(sigKeyFl, base64.StdEncoding.EncodeToString(sigKey)); erx != nil {
		return erx
	}

	if _, err := os.Stat(encKeyFl); err == nil || !errors.Is(err, os.ErrNotExist) {
		return fmt.Errorf("%v already exists", encKeyFl)
	}
	if erx := write(encKeyFl, string(encKey)); erx != nil {
		return erx
	}
	/*
		if ek, err := json.Marshal(encKey); err != nil {
			return nil
		} else {
			if erx := write(encKeyFl, string(ek)); erx != nil {
				return erx
			}
		} */
	return nil

}

func write(fl, data string) error {
	f, err := os.Create(fl)

	if err != nil {
		return err
	}

	defer f.Close()

	_, err = f.WriteString(data)

	if err != nil {
		return err
	}

	return nil
}

func genKeys(cfg *OndcConfig, months int) (sigKey, encKey []byte, err error) {
	cfg.ValidFrom = time.Now()
	cfg.Created = cfg.ValidFrom
	cfg.ValidUntil = cfg.Created.AddDate(0, months, 0)
	// https://docs.google.com/document/d/1MK_TvT8DCyd-ktA9hNLcgHG6Cbvo31cHv67VXA2kIiY[Technical Note - Participants joining ONDC: "Algorithms used for digital signature and encryption/decryption are "Ed25519" and "X25519" respectively."]
	//
	if cfg.SigningPubKey, sigKey, err = ed25519.GenerateKey(nil); err != nil {
		return
	}
	var xPvtKey *x25519.PrivateKey
	if xPvtKey, err = x25519.GenerateKey(cryptorand.Reader); err != nil {
		return
	}
	cfg.EncrPubKey = xPvtKey.PublicKey.Bytes()
	//encKey = xPvtKey.Bytes()
	encKey = xPvtKey.MarshalPEM()

	return
}

func fill() *OndcConfig {
	var cfg = &OndcConfig{
		Country:       country,
		City:          city,
		Type:          typ,
		SubscriberID:  subsID,
		SubscriberURL: subsURL,
		Domain:        domain,
	}
	if !(typ == "BPP" || typ == "BAP") {
		log.Printf("param type can be either BPP or BPP, found %v, defaulting to BAP", typ)
		cfg.Type = "BAP"
	}
	return cfg
}
