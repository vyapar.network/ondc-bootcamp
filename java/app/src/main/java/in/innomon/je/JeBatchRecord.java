/*
Copyright (c) 2019, QzIP Blockchain Technology LLP

http://www.apache.org/licenses/LICENSE-2.0

Author: Ashish Banerjee, <ashish@qzip.in>


 */
package in.innomon.je;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import java.io.Serializable;

/**
 *
 * @author ashish
 */
@Entity
public class JeBatchRecord implements Serializable {

    @PrimaryKey
    public String txnID;
    public String className;
    public String serializedJSON; // Gson 

    public JeBatchRecord() {
    }

}
