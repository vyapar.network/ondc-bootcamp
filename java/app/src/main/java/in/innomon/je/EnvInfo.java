/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2010 Oracle and/or its affiliates. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License.  You can
 * obtain a copy of the License at
 * https://glassfish.dev.java.net/public/CDDL+GPL_1_1.html
 * or packager/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at packager/legal/LICENSE.txt.
 *
 * GPL Classpath Exception:
 * Oracle designates this particular file as subject to the "Classpath"
 * exception as provided by Oracle in the GPL Version 2 section of the License
 * file that accompanied this code.
 *
 * Modifications:
 * If applicable, add the following below the License Header, with the fields
 * enclosed by brackets [] replaced by your own identifying information:
 * "Portions Copyright [year] [name of copyright owner]"
 *
 * Contributor(s):
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 * 
 * Portions Copyrighted 2011 Ashish Banerjee
 * Ashish Banerjee elects to include this software in this distribution under the  GPL Version 2 license. 
 */

package in.innomon.je;

/**
 *
 * @author ashish
 * 22-May-11: Added BDB JE Replication extensions 
 */
public class EnvInfo {
    private String envHome = "/home/ashish/abpay";
    private String balanceStore = "CashBalance";
    private String txnStore = "TxnStore";
    private boolean createAccountOnTxn = false;  // create a new UUID if not present

    // replication support
    private String groupName = "GpayReplicationGroup";
    private String nodeName  = "Master";
    private String nodeHostPort = "localhost:4101";
    private String helperHosts = "localhost:4101";
    
    public String getTxnStore() {
        return txnStore;
    }

    public void setTxnStore(String txnStore) {
        this.txnStore = txnStore;
    }

    public String getBalanceStore() {
        return balanceStore;
    }

    public void setBalanceStore(String balanceStore) {
        this.balanceStore = balanceStore;
    }

    public boolean isCreateAccountOnTxn() {
        return createAccountOnTxn;
    }

    public void setCreateAccountOnTxn(boolean createAccountOnTxn) {
        this.createAccountOnTxn = createAccountOnTxn;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getHelperHosts() {
        return helperHosts;
    }

    public void setHelperHosts(String helperHosts) {
        this.helperHosts = helperHosts;
    }

    public String getNodeHostPort() {
        return nodeHostPort;
    }

    public void setNodeHostPort(String nodeHostPort) {
        this.nodeHostPort = nodeHostPort;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getEnvHome() {
        return envHome;
    }

    public void setEnvHome(String envHome) {
        this.envHome = envHome;
    }

}
