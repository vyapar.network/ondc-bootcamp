package ondc;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Base64;

import org.bouncycastle.crypto.Signer;
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.bouncycastle.crypto.signers.Ed25519Signer;
import org.bouncycastle.jce.provider.BouncyCastleProvider;


public class Crypto {

    // based on 'GeneratePayloadClTest.java' shared by ONDC team during Agri Hackathon july 2022
    public static String blakeHash(String msg) throws Exception {
        // code snippet taken from test java file shared by ONDC team
        MessageDigest digest = MessageDigest.getInstance("BLAKE2B-512", BouncyCastleProvider.PROVIDER_NAME);
        digest.reset();
        digest.update(msg.getBytes(StandardCharsets.UTF_8));
        byte[] hash = digest.digest();
        String bs64 = Base64.getEncoder().encodeToString(hash);
        // System.out.println(bs64);
        return bs64;
    }

    public static String generateSignature(String req, String pk) throws Exception {
        String signature = null;

        Ed25519PrivateKeyParameters privateKey = new Ed25519PrivateKeyParameters(
        Base64.getDecoder().decode(pk.getBytes()), 0);
        Signer sig = new Ed25519Signer();
        sig.init(true, privateKey);
        sig.update(req.getBytes(), 0, req.length());
        byte[] s1 = sig.generateSignature();
        signature = Base64.getEncoder().encodeToString(s1);

        return signature;
    }
    public static String generateBlakeHash(String req) throws Exception{
   
        MessageDigest digest = MessageDigest.getInstance("BLAKE2B-512",BouncyCastleProvider.PROVIDER_NAME);
        digest.reset();
        digest.update(req.getBytes(StandardCharsets.UTF_8));
        byte[] hash = digest.digest();
        String bs64 = Base64.getEncoder().encodeToString(hash);
        System.out.println(bs64);
        return bs64;
  
    }
}