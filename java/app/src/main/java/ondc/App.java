package ondc;

import java.io.IOException;
import java.io.InputStream;
import twister.system.BDLParser;

public class App {

    public void Exec(String bdlfile) throws IOException {
        BDLParser cmds = new BDLParser();
        InputStream bdl = cmds.getClass().getClassLoader().getResourceAsStream(bdlfile);
        cmds.exec(bdl);
    }
    public static void main(String[] args) { 
    if (args.length != 2) {
        String app = (args.length == 0)? "App": args[0];
        System.err.println("usage "+ app +" <bdl file name>");       
        System.exit(1);
    }
    try {
        (new App()).Exec(args[1]);
    } catch(IOException e) {
        System.err.println(e);
    }
    System.exit(0);          
    }
}
